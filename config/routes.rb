Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :platforms do
    member do
      get '/days/:date', action: :by_date
      get '/days', action: :by_range_date
    end
  end
end
